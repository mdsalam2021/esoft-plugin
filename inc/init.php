<?php
/**
 * @package Esoft
 */

namespace Inc;

final class Init
{

    /**
     *  Store all the class inside an array
     * @return array Full list of classes name
     **/
    public static function get_services()
    {
        return [
            Pages\Dashboard::class,
            Base\Enqueue::class,
            Base\SettingsLinks::class,
            Base\CustomPostTypeController::class,
            Base\CustomTaxonomyController::class,
            Base\GalleryController::class,
            Base\TestimonialController::class,
            Base\TemplateController::class,
            Base\WidgetController::class,
            Base\AuthController::class,
        ];
    }

    /**
     *  Look through the classes, intialize them, and call the register() methond of it existis
     * @return
     *
     * */
    public static function register_services()
    {
        foreach (self::get_services() as $class) {
            $service = self::instantiate($class);
            if (method_exists($service, 'register')) {
                $service->register();
            }
        }
    }

    /**
     * Initialize the Class
     * @param class $class form the services array
     * @return class instance new Instance of the class
     */
    private static function instantiate($class)
    {
        // $service = new Pages\\Admin::class;
        $service = new $class();
        return new $class();
    }
}
