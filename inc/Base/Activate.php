<?php
/**
 * @package  Esoft
 */
namespace Inc\Base;

class Activate
{
    public static function activate() {
        flush_rewrite_rules();

        if( get_option('esoft_plugin') ){
            return;
        }

        $default = array();

        update_option('esoft_plugin', $default);

    }
}