<?php
/**
 * @pakage Esfot
 *
 **/

namespace Inc\Base;
use Inc\Base\BaseControlller;

class Enqueue extends BaseController
{

    function register()
    {
        add_action('admin_enqueue_scripts', array($this, 'enqueue'));

    }


    function enqueue()
    {
        // Enque all our scripts here
        wp_enqueue_style('mypluginstyle', $this->plugin_url . '/assets/mystyle.css');
        wp_enqueue_script('mypluginscript', $this->plugin_url . 'assets/myscript.js');
    }


}