<?php
/**
* @pakage Esoft Plugin
*/

namespace Inc\Api\Callbacks;


class CptCallbacks{

    public function cptSectionManager(){
        echo 'Create as many Custom post types as you want';
    }

    public function cptSanitize( $input ){
//
//        $output = get_option('esoft_plugin_cpt');
//
//        $neinput = array( $input['post_type'] => $input );

        return $input;

    }


    public function textField( $args ){
        $name = $args['label_for'];
        $option_name = $args['option_name'];
        $field = get_option( $option_name );
       // $value = $field[$name];

        echo '<input type="text" class="regular-text" id="' . $name . '" name="' . $option_name . '['. $name .']" value="" placeholder="'. $args['placeholder'] .'">';
    }


    public function checkboxField( $args )
    {

        $name = $args['label_for'];
        $classes = $args['class'];
        $option_name = $args['option_name'];
        $checkbox = get_option( $option_name );

        echo '<div class="' . $classes . '"><input type="checkbox" id="' . $name . '" name="'. $option_name .'[' . $name . ']" value="1" class=""><label for="' . $name . '"><div></div></label></div>';
    }


}