<?php
/*
 * @package Esoft
 *
 *  Triger this file when this plugin will be unstall
 */

if(! defined('WP_UNINSTALL_PLUGIN')){
 die();
}


// Acess the database via SQL
global $wpdb;
//
//$wpdb->query( "DELETE FROM wp_posts WHERE post_type = 'book'" );

$wpdb->query( "DELETE FROM wp_postmeta WHERE post_id NOT IN (SELECT id FROM wp_posts) " );